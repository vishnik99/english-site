$(document).ready(function(){
  $('.reviews-container').slick({
    accessibility: false,
    arrows: false,
    infinite: false,
    autoplay: true,
    autoplaySpeed: 3000

  });
  var slider = document.body.querySelectorAll(".progress-bar__slider")[0];
  var leftShift = 0;
  slider.style.left = "0px";
 $('.reviews-container').on('beforeChange', function(event,slick, currentSlide, nextSlide){
  	if(nextSlide > currentSlide) {
  		leftShift += 20;
 			slider.style.left = leftShift + "%";
  	} else {
  		leftShift -= 20;
 			slider.style.left = leftShift + "%";
  	}
});
});
